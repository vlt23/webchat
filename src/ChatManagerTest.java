package es.sidelab.webchat;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.CompletionService;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;

import es.codeurjc.webchat.Chat;
import es.codeurjc.webchat.ChatManager;
import es.codeurjc.webchat.User;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ChatManagerTest {

    @Test
    public void newChat() throws InterruptedException, TimeoutException {

        // Crear el chat Manager
        ChatManager chatManager = new ChatManager(5);

        // Crear un usuario que guarda en chatName el nombre del nuevo chat
        final String[] chatName = new String[1];

        chatManager.newUser(new TestUser("user") {
            public void newChat(Chat chat) {
                chatName[0] = chat.getName();
            }
        });

        // Crear un nuevo chat en el chatManager
        chatManager.newChat("Chat", 5, TimeUnit.SECONDS);

        // Comprobar que el chat recibido en el método 'newChat' se llama 'Chat'
        assertEquals("The method 'newChat' should be invoked with 'Chat', but the value is "
                + chatName[0], "Chat", chatName[0]);
    }

    @Test
    public void newUserInChat() throws InterruptedException, TimeoutException {

        ChatManager chatManager = new ChatManager(5);

        final String[] newUser = new String[1];

        TestUser user1 = new TestUser("user1") {
            @Override
            public void newUserInChat(Chat chat, User user) {
                newUser[0] = user.getName();
            }
        };

        TestUser user2 = new TestUser("user2");

        chatManager.newUser(user1);
        chatManager.newUser(user2);

        Chat chat = chatManager.newChat("Chat", 5, TimeUnit.SECONDS);

        chat.addUser(user1);
        chat.addUser(user2);

        assertEquals("Notified new user '" + newUser[0] + "' is not equal than user name 'user2'", "user2", newUser[0]);

    }

    @Test
    public void improvement1() throws InterruptedException {
        int numThreads = 4;
        ChatManager chatManager = new ChatManager(5);
        // Debug
        Lock lock = new ReentrantLock();

        ExecutorService executor = Executors.newFixedThreadPool(numThreads);
        try {
            CompletionService<TestUser> completionService = new ExecutorCompletionService<>(executor);

            for (int i = 0; i < numThreads; i++) {
                int numThread = i;
                completionService.submit(() -> execution1(numThread, chatManager, lock));
            }
            for (int i = 0; i < numThreads; i++) {
                Future<TestUser> userFuture = completionService.take();
                userFuture.get();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }

    private TestUser execution1(int numThread, ChatManager chatManager, Lock lock)
            throws InterruptedException {
        TestUser user = new TestUser("user" + numThread) {
            @Override
            public void newChat(Chat chat) {
                System.out.print(this.getName() + ": ");
                super.newChat(chat);
            }
            @Override
            public void newUserInChat(Chat chat, User user) {
                System.out.print(this.getName() + ": ");
                super.newUserInChat(chat, user);
            }
        };
        chatManager.newUser(user);

        try {
            for (int i = 0; i < 5; i++) {
                Chat chat = chatManager.newChat("chat" + i, 5, TimeUnit.SECONDS);
                chat.addUser(user);
                Collection<User> users = chat.getUsers();
                lock.lock();
                System.out.println("----------------------");
                System.out.print(chat.getName() + " -- ");
                System.out.println(user.getName() + ": ");
                for (User userIter : users) {
                    System.out.println(userIter.getName());
                }
                lock.unlock();
                System.out.println("----------------------");
            }
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Test
    public void chatTimeoutException() {
        ChatManager chatManager = new ChatManager(3);
        int numThreads = 5;
        boolean testOk = false;

        ExecutorService executor = Executors.newFixedThreadPool(numThreads);
        try {
            CompletionService<Chat> completionService = new ExecutorCompletionService<>(executor);
            for (int i = 0; i < numThreads; i++) {
                int currentNumThread = i;
                completionService.submit(() -> chatManager.newChat("chat" + currentNumThread,
                        10, TimeUnit.SECONDS));
            }

            for (int i = 0; i < numThreads; i++) {
                try {
                    Thread.sleep(1000);
                    Future<Chat> chatFuture = completionService.take();
                    Chat chat = chatFuture.get();
                    if (i < 1) {
                        chat.close();
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                    testOk = true;
                }
            }
        } finally {
            executor.shutdown();
        }

        assertTrue("The chatTimeoutException test is failed", testOk);
    }

    @Test
    public void noChatTimeoutException() {
        ChatManager chatManager = new ChatManager(5);
        int numThreads = 7;
        boolean testOk = true;

        ExecutorService executor = Executors.newFixedThreadPool(numThreads);
        try {
            CompletionService<Chat> completionService = new ExecutorCompletionService<>(executor);
            for (int i = 0; i < numThreads; i++) {
                int currentNumThread = i;
                completionService.submit(() -> chatManager.newChat("chat" + currentNumThread,
                        10, TimeUnit.SECONDS));
            }

            for (int i = 0; i < numThreads; i++) {
                try {
                    Thread.sleep(1000);
                    Future<Chat> chatFuture = completionService.take();
                    Chat chat = chatFuture.get();
                    if (i < 2) {
                        chat.close();
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                    testOk = false;
                }
            }
        } finally {
            executor.shutdown();
        }

        assertTrue("The noChatTimeoutException test is failed", testOk);
    }

    @Test
    public void parallelNotification() throws InterruptedException, TimeoutException {
        ChatManager chatManager = new ChatManager(1);
        Chat chat = chatManager.newChat("chat", 5, TimeUnit.SECONDS);
        int numUsers = 4;
        long timeStart = System.currentTimeMillis();

        CountDownLatch threadReceiveOK = new CountDownLatch(numUsers - 1);
        ExecutorService executor = Executors.newFixedThreadPool(numUsers);
        try {
            CompletionService<User> completionService = new ExecutorCompletionService<>(executor);
            for (int i = 0; i < numUsers; i++) {
                int currentNumThread = i;
                completionService.submit(() -> executionParallelNotification(currentNumThread, threadReceiveOK, chat));
            }

            for (int i = 0; i < numUsers; i++) {
                Future<User> userFuture = completionService.take();
                userFuture.get();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
        long timeEnd = System.currentTimeMillis() - timeStart;

        // Users send 4 messages, max 5 seconds
        assertTrue("The parallelNotification test is failed", timeEnd < 5000);
    }

    private User executionParallelNotification(int numThread, CountDownLatch threadReceiveOK, Chat chat)
            throws InterruptedException {
        TestUser testUser = new TestUser("user" + numThread) {
            @Override
            public void newMessage(Chat chat, User user, String message) {
                super.newMessage(chat, user, message);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // Nothing to do
                }
            }
        };
        chat.addUser(testUser);

        if (numThread == 0) {
            threadReceiveOK.await();
            for (int i = 0; i < 4; i++) {
                chat.sendMessage(testUser, "Hello World " + i + "!");
            }
        } else {
            threadReceiveOK.countDown();
        }

        return testUser;
    }

    @Test
    public void messageOrder() throws InterruptedException, TimeoutException {
        ChatManager chatManager = new ChatManager(1);
        Chat chat = chatManager.newChat("chat", 2, TimeUnit.SECONDS);
        CountDownLatch count = new CountDownLatch(1);
        boolean testOk1 = false;
        boolean testOk2 = false;

        int numUsers = 2;
        ExecutorService executor = Executors.newFixedThreadPool(numUsers);
        Exchanger<Boolean> exchanger1 = new Exchanger<>();
        Exchanger<Boolean> exchanger2 = new Exchanger<>();

        try {
            for (int i = 0; i < numUsers; i++) {
                int currentUser = i;
                executor.submit(() -> executionMessageOrder(currentUser, exchanger1, exchanger2, chat, count));
            }

            Future<Boolean> future1 = executor.submit(() -> checkExchanger(exchanger1));
            testOk1 = future1.get();
            Future<Boolean> future2 = executor.submit(() -> checkExchanger(exchanger2));
            testOk2 = future2.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }

        assertTrue("The messageOrder test is failed", testOk1 && testOk2);
    }

    private User executionMessageOrder(int numUser, Exchanger<Boolean> exchanger1, Exchanger<Boolean> exchanger2,
                                       Chat chat, CountDownLatch count) throws InterruptedException {
        final String[] messagesSend = {"0", "1", "2", "3", "4"};

        TestUser testUser = new TestUser("user" + numUser) {
            String[] messages = new String[5];
            int position = 0;

            @Override
            public void newMessage(Chat chat, User user, String message) {
                System.out.print(this.getName() + ": ");
                super.newMessage(chat, user, message);
                messages[position] = message;
                position++;
                try {
                    Thread.sleep((long) (Math.random() * 5000));

                    if (position == 5) {
                        if (numUser == 0) {
                            exchanger1.exchange(Arrays.equals(messagesSend, messages));
                        } else {
                            exchanger2.exchange(Arrays.equals(messagesSend, messages));
                        }
                    }
                } catch (InterruptedException e) {
                    // Nothing to do
                }
            }
        };
        chat.addUser(testUser);

        if (numUser == 1) {
            count.await();
            for (int i = 0; i < 5; i++) {
                chat.sendMessage(testUser, i + "");
            }
        } else {
            count.countDown();
        }

        return testUser;
    }

    private boolean checkExchanger(Exchanger<Boolean> exchanger) throws InterruptedException {
        return exchanger.exchange(null);
    }

    @Test
    public void improvement51() throws InterruptedException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        CompletionService<Chat> completionService = new ExecutorCompletionService<>(executor);
        int numChats = 15;
        ChatManager chatManager = new ChatManager(numChats);

        TestUser user1 = new TestUser("user1") {
            @Override
            public void newChat(Chat chat) {
                System.out.print("user1 ");
                super.newChat(chat);
            }
        };
        chatManager.newUser(user1);

        TestUser user2 = new TestUser("user2") {
            @Override
            public void newChat(Chat chat) {
                System.out.print("user2 ");
                super.newChat(chat);
            }
        };
        chatManager.newUser(user2);

        try {
            for (int i = 0; i < numChats; i++) {
                int currentChat = i;
                completionService.submit(() -> chatManager.newChat("chat" + currentChat, 5, TimeUnit.SECONDS));
            }

            for (int i = 0; i < numChats; i++) {
                Future<Chat> future = completionService.take();
                future.get();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }

    @Test
    public void improvement52() throws InterruptedException, TimeoutException, ExecutionException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        CompletionService<User> completionService = new ExecutorCompletionService<>(executor);
        ChatManager chatManager = new ChatManager(10);
        Chat chat = chatManager.newChat("chat", 5, TimeUnit.SECONDS);

        try {
            for (int i = 0; i < 10; i++) {
                int currentUser = i;
                completionService.submit(() -> execution52(currentUser, chat));
            }

            for (int i = 0; i < 10; i++) {
                Future<User> userFuture = completionService.take();
                userFuture.get();
            }
        } finally {
            executor.shutdown();
        }
    }

    private User execution52(int currentUser, Chat chat) {
        TestUser user = new TestUser("user" + currentUser);
        chat.addUser(user);
        return user;
    }

    @Test
    public void improvement53() throws InterruptedException, TimeoutException {
        this.messageOrder();
    }

}
