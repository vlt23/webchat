package es.codeurjc.webchat;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Chat {

    private final String name;
    private final ConcurrentMap<String, User> users = new ConcurrentHashMap<>();

    private final ChatManager chatManager;

    private final Lock chatConstructorLock = new ReentrantLock();
    private final ConcurrentHashMap<User, BlockingQueue<Message>> messageQueue = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<User, ExecutorService> executorServiceThread = new ConcurrentHashMap<>();
    //private ExecutorService executorService = Executors.newCachedThreadPool();

    private static final String newUserInChat = "newUserInChat";
    private static final String userExitedFromChat = "userExitedFromChat";

    public Chat(ChatManager chatManager, String name) {
        chatConstructorLock.lock();
        this.chatManager = chatManager;
        this.name = name;
        chatConstructorLock.unlock();
    }

    public String getName() {
        return name;
    }

    public void addUser(User userAdd) {
        messageQueue.put(userAdd, new ArrayBlockingQueue<>(64));
        executorServiceThread.put(userAdd, Executors.newSingleThreadExecutor());

        try {
            Future f = null;
            int numUsers;

            synchronized (users) {
                users.put(userAdd.getName(), userAdd);
                numUsers = users.size();

                // Si el usuario actual se borra justo antes del notify
                for (User u : users.values()) {
                    if (u != userAdd) {
                        messageQueue.get(u).put(new Message(u, Chat.newUserInChat));
                        f = executorServiceThread.get(u).submit(() -> takeMessage(u, userAdd));
                        //u.newUserInChat(this, user);
                    }
                }
            }

            for (int i = 0; i < numUsers - 1; i++) {
                f.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            //
        }
    }

    public void removeUser(User userRemove) {
        try {
            Future f = null;
            int numUsers;

            synchronized (users) {
                users.remove(userRemove.getName());
                numUsers = users.size();

                // Si entra un usuario cuando está notificando, o incluso se borra otro usuario
                for (User u : users.values()) {
                    messageQueue.get(u).put(new Message(u, Chat.userExitedFromChat));
                    f = executorServiceThread.get(u).submit(() -> takeMessage(u, userRemove));
                    //u.userExitedFromChat(this, user);
                }
            }

            for (int i = 0; i < numUsers; i++) {
                f.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            //
        }

        messageQueue.remove(userRemove);
        executorServiceThread.get(userRemove).shutdown();
    }

    public Collection<User> getUsers() {
        synchronized (users) {
            return Collections.unmodifiableCollection(users.values());
        }
    }

    public User getUser(String name) {
        return users.get(name);
    }

    public void sendMessage(User userSend, String message) {
        // Parallel
        try {
            Future f = null;
            int numUsers;

            synchronized (users) {
                numUsers = users.size();
                for (User u : users.values()) {
                    messageQueue.get(u).put(new Message(u, message));
                    f = executorServiceThread.get(u).submit(() -> takeMessage(u, userSend));
                }
                /*for (User u : users.values()) {
                    if (u != user) {
                        f.get();
                    }
                }*/
            }
            for (int i = 0; i < numUsers - 1; i++) {
                f.get();
            }
        } catch (InterruptedException | ExecutionException | NullPointerException e) {
            // Because chatHandler doesn't catch Exception
        }

        // Sequential
        /*synchronized (users) {
            for (User u : users.values()) {
                // Current user shouldn't receive his message
                if (u != user) {
                    u.newMessage(this, user, message);
                }
            }
        }*/
    }

    private void takeMessage(User u, User user) {
        try {
            // Each user check his queue
            synchronized (messageQueue.get(u)) {
                Message message = messageQueue.get(u).take();

                if (message.getMessage().equals(Chat.newUserInChat)) {
                    u.newUserInChat(this, user);
                } else if (message.getMessage().equals(Chat.userExitedFromChat)) {
                    u.userExitedFromChat(this, user);
                } else {
                    u.newMessage(this, user, message.getMessage());
                }
            }
        } catch (InterruptedException e) {
            //
        }
    }

    public void close() {
        this.chatManager.closeChat(this);
    }

}
