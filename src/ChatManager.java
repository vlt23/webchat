package es.codeurjc.webchat;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ChatManager {

    private final ConcurrentMap<String, Chat> chats = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, User> users = new ConcurrentHashMap<>();
    private final int maxChats;
    private Semaphore chatsPermits;

    private ReentrantLock timeoutLock = new ReentrantLock(true);
    private Condition timeoutCondition = timeoutLock.newCondition();

    private final ConcurrentHashMap<User, BlockingQueue<Message>> messageQueue = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<User, ExecutorService> executorServiceThread = new ConcurrentHashMap<>();
    private static final String newChat = "newChat";
    private static final String chatClose = "chatClose";

    public ChatManager(int maxChats) {
        // No hace falta exclusión mutua, en principio solo un hilo crea el ChatManager
        this.maxChats = maxChats;
        //chatsPermits = new Semaphore(maxChats);
    }

    public void newUser(User user) {

        executorServiceThread.put(user, Executors.newSingleThreadExecutor());
        if (users.containsKey(user.getName())) {
            throw new IllegalArgumentException("There is already a user with name \'"
                    + user.getName() + "\'");
        } else {
            users.put(user.getName(), user);
        }
        messageQueue.put(user, new ArrayBlockingQueue<>(64));
    }

    public Chat newChat(String name, long timeout, TimeUnit unit) throws InterruptedException,
            TimeoutException {

        Future f = null;
        Chat newChat = null;
        int numUsers = 0;

        timeoutLock.lock();
        try {
            if (chats.containsKey(name)) {
                return chats.get(name);
            } else {

                if (chats.size() == maxChats) {
                    // Devuelve false si llega a timeout
                    //boolean isContinue = chatsPermits.tryAcquire(timeout, unit);
                    //TODO: fix spurious wakeup
                    boolean isContinue = timeoutCondition.await(timeout, unit);
                    if (!isContinue) {
                        throw new TimeoutException("There is no enough capacity to create a new chat");
                    }

                } else {
                    newChat = new Chat(this, name);
                    chats.put(name, newChat);

                    synchronized (users) {
                        numUsers = users.size();
                        timeoutLock.unlock();

                        for (User user : users.values()) {
                            Chat auxChat = newChat;
                            messageQueue.get(user).put(new Message(user, ChatManager.newChat));
                            f = executorServiceThread.get(user).submit(() -> takeNotifications(user, auxChat));
                            //user.newChat(newChat);
                        }
                    }
                }
            }
        } finally {
            if (timeoutLock.isHeldByCurrentThread()) {
                timeoutLock.unlock();
            }
        }

        try {
            for (int i = 0; i < numUsers; i++) {
                f.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            //
        }

        return newChat;
    }

    public void closeChat(Chat chat) {
        Chat removedChat;

        Future f = null;
        timeoutLock.lock();
        int numUsers;

        try {
            removedChat = chats.remove(chat.getName());
            if (removedChat == null) {
                throw new IllegalArgumentException("Trying to remove an unknown chat with name \'"
                        + chat.getName() + "\'");
            }

            //chatsPermits.release();
            if (timeoutLock.hasWaiters(timeoutCondition)) {
                timeoutCondition.signal();
            }

            synchronized (users) {
                numUsers = users.size();
                timeoutLock.unlock();

                try {
                    for (User user : users.values()) {
                        messageQueue.get(user).put(new Message(user, ChatManager.chatClose));
                        f = executorServiceThread.get(user).submit(() -> takeNotifications(user, removedChat));
                        //user.chatClosed(removedChat);
                    }
                } catch (InterruptedException e) {
                    //
                }
            }
        } finally {
            if (timeoutLock.isHeldByCurrentThread()) {
                timeoutLock.unlock();
            }
        }

        try {
            for (int i = 0; i < numUsers; i++) {
                f.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            //
        }
    }

    public Collection<Chat> getChats() {
        synchronized (chats) {
            return Collections.unmodifiableCollection(chats.values());
        }
    }

    public Chat getChat(String chatName) {
        return chats.get(chatName);
    }

    public Collection<User> getUsers() {
        synchronized (users) {
            return Collections.unmodifiableCollection(users.values());
        }
    }

    public User getUser(String userName) {
        return users.get(userName);
    }

    public void close() {
    }

    private void takeNotifications(User user, Chat chat) {
        try {
            // Each user check his queue
            synchronized (messageQueue.get(user)) {
                Message message = messageQueue.get(user).take();

                if (message.getMessage().equals(ChatManager.newChat)) {
                    user.newChat(chat);
                } else {
                    user.chatClosed(chat);
                }
            }
        } catch (InterruptedException e) {
            //
        }
    }

}
